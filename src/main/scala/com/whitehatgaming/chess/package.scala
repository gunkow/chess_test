package com.whitehatgaming

import com.whitehatgaming.chess.Move

package object chess {
  type Board = Vector[Vector[Square]]
  type Square = Option[Piece]
  type Move = (Int, Int, Int, Int)

  val startBoard: Board = for (j <- (0 until 8).toVector)
    yield {
      (0 to 7).toVector map { i: Int =>
        (j, i) match {
          case (0, 0) => Some(Rook(Black))
          case (0, 7) => Some(Rook(Black))
          case (0, 1) => Some(Knight(Black))
          case (0, 6) => Some(Knight(Black))
          case (0, 2) => Some(Bishop(Black))
          case (0, 5) => Some(Bishop(Black))
          case (0, 3) => Some(Queen(Black))
          case (0, 4) => Some(King(Black))

          case (1, 0) => Some(Pawn(Black))
          case (1, 7) => Some(Pawn(Black))
          case (1, 1) => Some(Pawn(Black))
          case (1, 6) => Some(Pawn(Black))
          case (1, 2) => Some(Pawn(Black))
          case (1, 5) => Some(Pawn(Black))
          case (1, 3) => Some(Pawn(Black))
          case (1, 4) => Some(Pawn(Black))

          case (7, 0) => Some(Rook(White))
          case (7, 7) => Some(Rook(White))
          case (7, 1) => Some(Knight(White))
          case (7, 6) => Some(Knight(White))
          case (7, 2) => Some(Bishop(White))
          case (7, 5) => Some(Bishop(White))
          case (7, 3) => Some(Queen(White))
          case (7, 4) => Some(King(White))

          case (6, 0) => Some(Pawn(White))
          case (6, 7) => Some(Pawn(White))
          case (6, 1) => Some(Pawn(White))
          case (6, 6) => Some(Pawn(White))
          case (6, 2) => Some(Pawn(White))
          case (6, 5) => Some(Pawn(White))
          case (6, 3) => Some(Pawn(White))
          case (6, 4) => Some(Pawn(White))

          case _ => Option.empty[Piece]
        }
      }
    }
}

class UserInputIterator(input: UserInput) extends Iterator[Move] {
  var current: Array[Int] = input.nextMove()

  override def hasNext: Boolean = current != null

  override def next(): Move = {
    val prev = current
    current = input.nextMove()
    return (prev(0), prev(1), prev(2), prev(3))
  }
}