package com.whitehatgaming.chess

case class State(board: Board, currentPlayer: Player)

object State {
  val startState: State = State(startBoard, White)

  def applyMove(state: State, move: Move): State = move match {
    case (from_x, from_y, to_x, to_y) =>
      val newBoard =
        state.board.zipWithIndex.map {
          case (row: Vector[Square], y) => row.zipWithIndex.map {
            case (square: Square, x) => (x, y) match {
              case (`from_x`, `from_y`) => None
              case (`to_x`, `to_y`) => state.board(from_y)(from_x)
              case _ => square
            }
          }
        }
      State(newBoard, state.currentPlayer.opposite)
  }
}