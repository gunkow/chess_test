package com.whitehatgaming.chess

sealed abstract class Piece(player: Player) {
  def isWhite: Boolean = player.isWhite
  def getPlayer = player
}


case class Pawn(player: Player) extends Piece(player)
case class Rook(player: Player) extends Piece(player)
case class King(player: Player) extends Piece(player)
case class Bishop(player: Player) extends Piece(player)
case class Queen(player: Player) extends Piece(player)
case class Knight(player: Player) extends Piece(player)

