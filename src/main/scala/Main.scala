import com.whitehatgaming.UserInputFile
import com.whitehatgaming.chess.{Controller, Renderer, RulesMVP, State}

object Main extends App {
  val renderer = new Renderer

  println("Running chess on all the files in ./data/")

  val userInput = new UserInputFile("data/sample-moves.txt")
  val controller = new Controller(input = userInput, output = renderer,
    rules = RulesMVP, state = State.startState)
  println("------------2nd round___________")

  val userInput2 = new UserInputFile("data/sample-moves-invalid.txt")
  val controller2 = new Controller(input = userInput2, output = renderer,
    rules = RulesMVP, state = State.startState)
  println("------------3rd round___________")

  val userInput3 = new UserInputFile("data/checkmate.txt")
  val controller3 = new Controller(input = userInput2, output = renderer,
    rules = RulesMVP, state = State.startState)



}
