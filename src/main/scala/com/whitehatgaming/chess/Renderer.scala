package com.whitehatgaming.chess

trait Renderable {
  def report(what: ForbidMove): Unit

  def renderState(state: State): Unit
}

class Renderer extends Renderable {
  def renderState(state: State): Unit = println(makeString(state) + "\n")

  def makeString(state: State): String = {
    state.board.map(_.map {
      case None => '·'
      case Some(p: Piece) => p match {
        case piece: Pawn => if (piece.isWhite) 'P' else 'p'
        case piece: Rook => if (piece.isWhite) 'R' else 'r'
        case piece: Knight => if (piece.isWhite) 'N' else 'n'
        case piece: Bishop => if (piece.isWhite) 'B' else 'b'
        case piece: Queen => if (piece.isWhite) 'Q' else 'q'
        case piece: King => if (piece.isWhite) 'K' else 'k'
      }
    }.mkString(" | ").prepended("| ").appended(" |").mkString(""))
      .mkString("\n")
  }

  def reportMap(what: ForbidMove): String = what match {
    case NoMoveForbid(move) => "piece does not move"
    case EmptyCellForbid(move) => "no piece to move"
    case CheckKingForbid(move) => "king in danger"
    case WrongColorForbid(move) => "use piece of an opposite color"
    case TakeSamePlayerPieceForbid(move) => "cannot take piece of same color"
    case WrongMovePatternForbid(move) => s"move not allowed"

  }

  override def report(what: ForbidMove): Unit = println(reportMap(what) +
    s": (${what.move._1}, ${what.move._2}) -> (${what.move._3}, ${what.move._4})" )
}
