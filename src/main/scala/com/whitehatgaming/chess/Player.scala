package com.whitehatgaming.chess

sealed trait Player {
  def isWhite: Boolean
  def isBlack: Boolean = !isWhite
  def opposite: Player = if (this.isWhite) Black else White
}

object White extends Player {
  override def isWhite: Boolean = true
}

object Black extends Player {
  override def isWhite: Boolean = false
}
