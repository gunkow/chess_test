## chess game from file to console on scala:

**requirements**:

sbt.version = 1.3.10 <br>
scalaVersion := 2.13.1

**simply:**

- `sbt test` or
- `sbt run`

see something like: 
![](img/screen.png)
