import com.whitehatgaming.UserInputFile
import com.whitehatgaming.chess.{CheckKingForbid, Controller, ForbidMove, RulesMVP, Pawn, Piece, Renderer, State, WrongMovePatternForbid}
import org.scalatest._

class Speca extends FlatSpec {
  val start: State = State.startState

  "State" should "move piece and change player" in {
    assert(start.currentPlayer.isWhite)

    val move_white_pawn: State = State.applyMove(start, (0, 6, 0, 4))
    val pawn: Piece = move_white_pawn.board(4)(0).get

    assert(move_white_pawn.currentPlayer.isBlack)
    assert(move_white_pawn.board(6)(0).isEmpty)
    assert(pawn.isWhite)
    assert(pawn.isInstanceOf[Pawn])
  }

  "Rules" should "move pawn, knight, bishop, rook, queen / change player" in {
    val move_white_pawn: Either[ForbidMove, State] = RulesMVP.makeMove(start, (4, 6, 4, 4))
    val move_black_knight: Either[ForbidMove, State] = RulesMVP.makeMove(move_white_pawn.right.get, (1, 0, 0, 2))
    val move_white_bishop_kill_knight: Either[ForbidMove, State] = RulesMVP.makeMove(move_black_knight.right.get, (5, 7, 0, 2))
    val move_black_rook = RulesMVP.makeMove(move_white_bishop_kill_knight.right.get, (0, 0, 1, 0))
    val move_white_queen = RulesMVP.makeMove(move_black_rook.right.get, (3, 7, 7, 3))

    assert(move_white_pawn.isRight)
    assert(move_black_knight.isRight)
    assert(move_white_bishop_kill_knight.isRight)
    assert(move_black_rook.isRight)
    assert(move_white_queen.isRight)

    assert(move_white_pawn.right.get.currentPlayer.isBlack)
    assert(move_black_knight.right.get.currentPlayer.isWhite)
  }

  "Rules" should "ignore incorrect moves" in {
    val moveIncorrect_rook_jumpOver: Either[ForbidMove, State] = RulesMVP.makeMove(start, (0, 7, 0, 3))
    val moveIncorrect_knight_asBishop: Either[ForbidMove, State] = RulesMVP.makeMove(start, (1, 7, 4, 4))
    assert(moveIncorrect_rook_jumpOver == Left(WrongMovePatternForbid((0, 7, 0, 3))))
    assert(moveIncorrect_knight_asBishop == Left(WrongMovePatternForbid(1, 7, 4, 4)))

    val move_white_pawn: Either[ForbidMove, State] = RulesMVP.makeMove(start, (4, 6, 4, 4))
    val move_black_pawn: Either[ForbidMove, State] = RulesMVP.makeMove(move_white_pawn.right.get, (3, 1, 3, 3))
    val move_white_bishop: Either[ForbidMove, State] = RulesMVP.makeMove(move_black_pawn.right.get, (5, 7, 1, 3))
    val move_black_pawn_InCheck: Either[ForbidMove, State] = RulesMVP.makeMove(move_white_bishop.right.get, (0, 1, 0, 2))
    assert(move_black_pawn_InCheck == Left(CheckKingForbid(0, 1, 0, 2)))
  }

  "Controller" should "play from UserInput" in {

    val userInput = new UserInputFile("data/sample-moves.txt")
    val renderer = new Renderer

    println("RUNNING \"data/sample-moves.txt\"")

    val controller = new Controller(input = userInput, output = renderer,
      rules = RulesMVP, state = State.startState)

    assert(controller.messageLog.isEmpty)

    println("RUNNING \"data/sample-moves-invalid.txt\"")

    val userInput2 = new UserInputFile("data/sample-moves-invalid.txt")
    val controller2 = new Controller(input = userInput2, output = renderer,
      rules = RulesMVP, state = State.startState)

    println("RUNNING \"data/sample-moves-invalid.txt\"")

    val userInput3 = new UserInputFile("data/checkmate.txt")
    val controller3 = new Controller(input = userInput3, output = renderer,
      rules = RulesMVP, state = State.startState)

    assert(controller3.messageLog.isEmpty)
  }
}