package com.whitehatgaming.chess

sealed abstract class ForbidMove(val move: Move)

case class NoMoveForbid(override val move: Move) extends ForbidMove(move)
case class EmptyCellForbid(override val move: Move) extends ForbidMove(move)
case class CheckKingForbid(override val move: Move) extends ForbidMove(move)
case class WrongColorForbid(override val move: Move) extends ForbidMove(move)
case class TakeSamePlayerPieceForbid(override val move: Move) extends ForbidMove(move)
case class WrongMovePatternForbid(override val move: Move) extends ForbidMove(move)