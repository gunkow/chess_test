package com.whitehatgaming.chess

trait Rules {
  def makeMove(state: State, move: Move): Either[ForbidMove, State]
}

object RulesMVP extends Rules {
  override def makeMove(state: State, move: Move): Either[ForbidMove, State] = move match {
    case (from_x, from_y, to_x, to_y) =>
      val futureState: State = State.applyMove(state, move)
      val from_square: Square = state.board(from_y)(from_x)
      val to_square: Square = state.board(to_y)(to_x)
      if (from_x == to_x && from_y == to_y)
        return Left(NoMoveForbid(move))
      if (from_square.isEmpty)
        return Left(EmptyCellForbid(move))
      val currentPiece: Piece = from_square.get
      if (checkKingDanger(futureState))
        return Left(CheckKingForbid(move))
      if (currentPiece.getPlayer != state.currentPlayer)
        return Left(WrongColorForbid(move))
      if (to_square.fold(false)(piece => piece.getPlayer == currentPiece.getPlayer))
        return Left(TakeSamePlayerPieceForbid(move))
      if (!checkMovePiece(currentPiece, state, move, to_square.nonEmpty))
        return Left(WrongMovePatternForbid(move))

      Right(futureState)
  }

  def checkMovePiece(currentPiece: Piece, state: State, move: Move, isAttack: Boolean): Boolean =
    currentPiece match {
      case Pawn(White) => checkWhitePawnMovePattern(move, isAttack) && checkClearPath(state, move)
      case Pawn(Black) => checkBlackPawnMovePattern(move, isAttack) && checkClearPath(state, move)
      case Rook(_) => checkRookMovePattern(move) && checkClearPath(state, move)
      case Knight(_) => checkKnightMovePattern(move)
      case Bishop(_) => checkBishopMovePatern(move) && checkClearPath(state, move)
      case Queen(_) => checkQueenMovePattern(move) && checkClearPath(state, move)
      case King(_) => checkKingMove(move)
    }

  def checkWhitePawnMovePattern(move: Move, isAttack: Boolean): Boolean = move match {
    case (from_x, from_y, to_x, to_y) =>
      if (isAttack)
        (from_y - to_y == 1) && ((to_x - from_x).abs == 1)
      else
        from_y - to_y <= 2
  }

  def checkBlackPawnMovePattern(move: Move, isAttack: Boolean): Boolean = move match {
    case (from_x, from_y, to_x, to_y) =>
      if (isAttack)
        (from_y - to_y == -1) && ((to_x - from_x).abs == 1)
      else
        to_y - from_y <= 2
  }

  def checkClearPath(state: State, move: Move): Boolean = move match {
    case (from_x, from_y, to_x, to_y) =>
      val big_x = from_x max to_x
      val big_y = from_y max to_y
      val small_x = from_x min to_x
      val small_y = from_y min to_y

      val pathSquares: Seq[(Int, Int)] =
        if (big_x == small_x)
          (small_y + 1).until(big_y).map((_, big_x))
        else if (big_y == small_y)
          (small_x + 1).until(big_x).map((big_y, _))
        // x increasing -> y decreasing   and vice versa
        else if ((to_x - from_x) * (to_y - from_y) < 0)
          (small_y + 1).until(big_y) zip (big_x - 1).until(small_x).by(-1)
        else
          (small_y + 1).until(big_y) zip (small_x + 1).until(big_x)

      return pathSquares.forall {
        case (y, x) => state.board(y)(x).isEmpty
      }
  }

  def checkRookMovePattern(move: Move): Boolean = move match {
    case (from_x, from_y, to_x, to_y) => (from_x - to_x) == 0 || (from_y - to_y) == 0
  }

  def checkKnightMovePattern(move: Move): Boolean = move match {
    case (from_x, from_y, to_x, to_y) => (((to_x - from_x).abs == 1) && ((to_y - from_y).abs == 2)) ||
      (((to_x - from_x).abs == 2) && ((to_y - from_y).abs == 1))
  }

  def checkBishopMovePatern(move: Move): Boolean = move match {
    case (from_x, from_y, to_x, to_y) => (from_x - to_x).abs == (from_y - to_y).abs
  }

  def checkQueenMovePattern(move: Move): Boolean =
    checkBishopMovePatern(move) || checkRookMovePattern(move)

  def checkKingMove(move: Move): Boolean = move match {
    case (from_x, from_y, to_x, to_y) => (to_y - from_y).abs <= 1 && (to_x - from_x) <= 1
  }

  def checkKingDanger(state: State): Boolean = {
    val kingPosition: (Int, Int) = (for {
      y <- 0 to 7
      x <- 0 to 7
      piece <- state.board(y)(x)
      if piece.isInstanceOf[King] && state.currentPlayer != piece.getPlayer}
      yield (x, y))
      .head

    val enemyPieces: Seq[(Int, Int, Piece)] = for {
      y <- 0 to 7
      x <- 0 to 7
      piece <- state.board(y)(x)
      if state.currentPlayer == piece.getPlayer}
      yield (x, y, piece)

    enemyPieces.exists {
      case (x, y, piece) => checkMovePiece(piece, state, (x, y, kingPosition._1, kingPosition._2), isAttack = true)
    }
  }
}
