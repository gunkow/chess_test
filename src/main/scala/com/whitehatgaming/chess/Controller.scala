package com.whitehatgaming.chess

import com.whitehatgaming.{UserInput, UserInputIterator}
import scala.collection.mutable.ListBuffer

class Controller(input: UserInput, output: Renderable,
                 rules: Rules, var state: State = State.startState) {
  val userInputIterator = new UserInputIterator(input)
  val messageLog = ListBuffer.empty[ForbidMove]

  output.renderState(state)
  userInputIterator.foreach(move => {
    rules.makeMove(state, move) match {
      case Left(forbidMessage) => {messageLog.append(forbidMessage); output.report(forbidMessage)}
      case Right(newState) => state = newState
    }
    output.renderState(state)
  })
}
